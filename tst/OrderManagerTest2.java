import mockexample.Order;
import mockexample.OrderChecker;
import mockexample.OrderManager;
import mockexample.ProcessOrders;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class OrderManagerTest2 {

    private OrderManager orderManager;

    //use @Mock annotation to create a ProcessOrders mock object
    @Mock
    private ProcessOrders processOrdersMock;
    //use @Spy annotation to create a OrderChecker spy object
    @Spy
    private OrderChecker orderCheckerSpy;

    private Order validOrder, invalidOrder, blackListedOrder;

    @Before
    public void setup() {
        //GIVEN - setup mock and other necessary objects

        //create orderManager instance with mock object
        orderManager = new OrderManager(processOrdersMock);
        //instantiate validOrder and invalidOrder objects
        validOrder = new Order(1, "dr.im", "1000 uni center lane",10000.0, false);
        invalidOrder = new Order(2, "", "", 100, false);
    }


    @Test
    public void testCheckInvalidOrder() {
        //WHEN - have the mock object do something (that you want to test)

        //we expect this to be false
        //code in isValidOrder() will get executed because spy is used
        boolean result = orderCheckerSpy.isValidOrder(invalidOrder);
        //THEN - verify expected result
        Assert.assertFalse(result);

        //A spy will remember all interactions.
        //verify interactions when isValidOrder() was called
        Mockito.verify(orderCheckerSpy).isValidOrder(invalidOrder);
        Mockito.verifyNoMoreInteractions(orderCheckerSpy);
    }


    @Test
    public void testCheckValidOrder() {
        //WHEN - have the mock object do something (that you want to test)
        boolean orderResult = false;

        if (orderCheckerSpy.isValidOrder(validOrder)) {
            orderResult = orderManager.processOrder(Mockito.anyInt());
        }
        //code in isValidOrder() will get executed because spy is used
        Assert.assertTrue(orderResult);
        //THEN - verify expected result
        Mockito.verify(processOrdersMock).shipOrder(Mockito.anyInt());
        Mockito.verifyNoMoreInteractions(processOrdersMock);

        Mockito.verify(orderCheckerSpy).isValidOrder(validOrder);
        Mockito.verify(orderCheckerSpy).isProfitableSale(validOrder);
        Mockito.verify(orderCheckerSpy).isFromBannedCustomer(validOrder);
        Mockito.verifyNoMoreInteractions(orderCheckerSpy);
        //A spy will remember all interactions.
        //verify interactions when isValidOrder() was called

        //A mock will remember all interactions.
        //You can selectively verify that specific methods were actually called.

        //you can also verify that no other interactions on the mock object
        //happened other than the ones expected above
    }


    @Test
    public void testOrderFromBannedCustomer() {
        Mockito.when(orderCheckerSpy.isFromBannedCustomer(validOrder)).
                thenReturn(true);
        boolean result = orderCheckerSpy.isValidOrder(validOrder);
        Assert.assertFalse(result);

        Mockito.verify(orderCheckerSpy).isValidOrder(validOrder);
        Mockito.verify(orderCheckerSpy).isFromBannedCustomer(validOrder);
        Mockito.verify(orderCheckerSpy).isProfitableSale(validOrder);
        Mockito.verifyNoMoreInteractions(orderCheckerSpy);

    }




}
