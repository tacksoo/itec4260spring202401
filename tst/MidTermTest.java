import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.StringReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class MidTermTest {

    public static String CHEAP_SHARK_URL = "https://www.cheapshark.com/api/1.0/deals?storeID=1&upperPrice=15";
    public static String SCHEDULE_URL = "https://gist.githubusercontent.com/tacksoo/d1fcb51f8921cdc90d1ffadb0b63b768/raw/6c9a8b9ffadd87b4bd0217b91cdd90bb9e227ef2/schedule.csv";
    public static String FINANCE_URL = "https://gist.githubusercontent.com/tacksoo/b9edbfc8c03e1ca89d459bf1af39842d/raw/75abf553a0297d9202b1a568f185f735055d6f81/stonks.csv";
    public static String HACKER_NEWS_URL = "https://news.ycombinator.com/";

    public static WebDriver driver;
    @BeforeClass
    public static void setUp() {
        driver = new ChromeDriver();
    }

    @Test
    public void testCheapShark() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(new URL(CHEAP_SHARK_URL));
        int count = 0;
        for (int i = 0; i < root.size(); i++) {
            JsonNode currentNode = root.get(i);
            if (currentNode.get("steamRatingText").asText().equals("Very Positive")) {
                count += 1;
            }
        }
        Assert.assertTrue(count > 1);
        //System.out.println(count);
    }

    @Test
    public void testLines() throws Exception {
        String text = IOUtils.toString(new URL(SCHEDULE_URL));
        String[] lines = text.split("\n");
        Assert.assertEquals(29, lines.length);
    }

    @Test
    public void testLowestPercentage() throws Exception {
        String str = IOUtils.toString(new URL(FINANCE_URL));
        CSVParser parser = new CSVParser(new StringReader(str), CSVFormat.DEFAULT.builder().setHeader().build());
        double min = 0;
        for(CSVRecord record: parser) {
            String percent = record.get(" Prediction").replace("%","");
            min = Math.min(min, Double.parseDouble(percent));
        }
        FileUtils.writeStringToFile(new File("stonk.csv"),String.valueOf(min),"UTF-8");
        Assert.assertTrue(Files.exists(Path.of("stonk.csv")));
    }

    @Test
    public void testNews() {
        driver.get(HACKER_NEWS_URL);
        List<WebElement> newsItems = driver.findElements(By.className("rank"));
        Assert.assertEquals(30, newsItems.size());
    }

}
