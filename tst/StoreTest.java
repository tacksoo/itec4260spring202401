import org.junit.Assert;
import org.junit.Test;

public class StoreTest {

    public static String GAME_CSV_URL = "https://gist.githubusercontent.com/tacksoo/349702bd06852814fba06a4df48e32d8/raw/5fb59f716e9069ac186b1994376c85823a65e335/myinventory.csv";
    // update count if you add/delete rows from CSV file
    public static int EXPECTED_GAME_COUNT = 3;

    @Test
    public void loadFromWeb() {
        Inventory inv = new Inventory();
        Store myStore = new Store(inv);
        myStore.loadInventoryFromWeb(GAME_CSV_URL);
        Assert.assertEquals(EXPECTED_GAME_COUNT, inv.getSize());
    }

    // write test for the createGameFromRecord()
}
