import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

public class TuitionCalculatorTest {

    private static WebDriver driver;
    private final static String USNEWS_URL = "https://www.usnews.com/best-colleges/georgia-gwinnett-college-41429";
    private final static String CALC_URL = "http://www.ggc.edu/admissions/tuition-and-financial-aid-calculators/index.html#";
    private double EXPECTED_INSTATE_YEARLY_AMOUNT = 5262;
    private double EXPECTED_OUTOFSTATE_YEARLY_AMOUNT = 16244;


    @BeforeClass
    public static void setUp() {
        //ChromeOptions options = new ChromeOptions();
        //options.addArguments("--headless");
        driver = new ChromeDriver();

    }

    @Test
    public void testInStateTuition() {
        driver.get(CALC_URL);
        // inorout0 is out of state
        // inorout1 is in state
        WebElement inorout = driver.findElement(By.id("inorout1"));
        inorout.click();
        // select max credit hours
        Select creditHoursSelect = new Select(driver.findElement(By.id("creditHOURS")));
        creditHoursSelect.selectByValue("15");
        // get total cost
        WebElement totalCost = driver.findElement(By.id("totalcost"));
        String actualSemesterCost = totalCost.getAttribute("value");
        double semesterCost = Double.parseDouble(actualSemesterCost.replace("$",""));
        Assert.assertEquals(EXPECTED_INSTATE_YEARLY_AMOUNT, semesterCost*2, 0.01);
    }

    @Test
    public void testOutOfStateTuition() {
        driver.get(CALC_URL);
        // inorout0 is out of state
        // inorout1 is in state
        WebElement inorout = driver.findElement(By.id("inorout0"));
        inorout.click();
        // select max credit hours
        Select creditHoursSelect = new Select(driver.findElement(By.id("creditHOURS")));
        creditHoursSelect.selectByValue("15");
        // get total cost
        WebElement totalCost = driver.findElement(By.id("totalcost"));
        String actualSemesterCost = totalCost.getAttribute("value");
        double semesterCost = Double.parseDouble(actualSemesterCost.replace("$",""));
        Assert.assertEquals(EXPECTED_OUTOFSTATE_YEARLY_AMOUNT, semesterCost*2, 0.01);
    }

    @Test
    public void testMyTuition() {
        //log into banner
        //get your tuition bill
        //insert your condition into tuition calculator
        //assert
        // https://ggc.gabest.usg.edu/pls/B400/twbkwbis.P_WWWLogin

    }

    @Test
    @Ignore
    public void testUSNEWS() {
        driver.get(USNEWS_URL);
        //WebElement div = driver.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div[5]/div/div[1]/div/div/section/div[2]/div[2]/div/div[2]"));
        //System.out.println(div);
        //WebElement instatePriceYearly = driver.findElement(By.xpath(""));
        //System.out.println(instatePriceYearly);
    }

    @AfterClass
    public static void cleanUp() throws Exception {
        Thread.sleep(10000);
        driver.close();
    }
}
