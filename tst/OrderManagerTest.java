import mockexample.InvalidOrderException;
import mockexample.Order;
import mockexample.OrderManager;
import mockexample.ProcessOrders;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.InvalidClassException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

public class OrderManagerTest {

    private OrderManager orderManager;
    private ProcessOrders processorMock;


    @Before
    public void setup() {

        // create a ProcessOrders mock object
        processorMock = Mockito.mock(ProcessOrders.class);

        // create orderManager instance with mock object
        orderManager = new OrderManager(processorMock);
    }

    @Test
    public void testProcessValidOrder() {
        //***
        //GIVEN - setup mock and other necessary objects
        //***


        //***
        //WHEN - have the mock object do something (that you want to test)
        //***
        boolean result = orderManager.processOrder(Mockito.anyInt());

        //***
        //THEN - verify expected result
        //***
        Assert.assertTrue(result);
        //verfiy that shipOrder() has been called once
        Mockito.verify(processorMock).shipOrder(Mockito.anyInt());
        Mockito.verifyNoMoreInteractions(processorMock);

    }

    @Test
    public void testProcessInvalidOrder() {
        //***
        //GIVEN - setup mock and define expected behavior
        //***
        Mockito.doThrow(new InvalidOrderException()).when(processorMock).shipOrder(12345);

        //***
        //WHEN - have the mock object do something (that you want to test)
        //***
        boolean result = orderManager.processOrder(12345);

        //***
        //THEN - verify expected result
        //***
        Assert.assertFalse(result);
        Mockito.verify(processorMock).shipOrder(12345);
        Mockito.verifyNoMoreInteractions(processorMock);
    }


    @Test
    public void testProfitHappyCase() {
        //***
        //GIVEN - setup mock and define expected behavior
        //***
        Mockito.when(processorMock.getFraudulentOrders()).thenReturn(new ArrayList<>());

        //***
        //WHEN - have the mock object do something (that you want to test)
        //***
        orderManager.processOrder(1);
        orderManager.processOrder(2);
        orderManager.processOrder(3);
        orderManager.processOrder(4);
        double actualProfit = orderManager.calculateProfit();
        double expectedProfit = 4 * 1.11;


        //***
        //THEN - verify expected result
        //***
        Assert.assertEquals(expectedProfit, actualProfit, 0);
        Mockito.verify(processorMock).shipOrder(1);
        Mockito.verify(processorMock).shipOrder(2);
        Mockito.verify(processorMock).shipOrder(3);
        Mockito.verify(processorMock).shipOrder(4);
        Mockito.verify(processorMock, Mockito.times(2)).getFraudulentOrders();
        Mockito.verifyNoMoreInteractions(processorMock);

    }


    @Test
    public void testProfitWithFrauds() {
        //***
        //GIVEN - setup mock and define expected behavior
        //***
        List<Order> expectedFraudOrders = new ArrayList<>();
        expectedFraudOrders.add(new Order(4));
        expectedFraudOrders.add(new Order(13));
        expectedFraudOrders.add(new Order(1984));
        Mockito.when(processorMock.getFraudulentOrders()).
                thenReturn(expectedFraudOrders);

        //***
        //WHEN - have the mock object do something (that you want to test)
        //***
        orderManager.processOrder(1);
        orderManager.processOrder(2);
        orderManager.processOrder(3);
        orderManager.processOrder(4); //fraudulent
        orderManager.processOrder(13); //fraudulent
        orderManager.processOrder(1984); //fraudulent

        double actualProfit = orderManager.calculateProfit();
        double expectedProfit = (3 * 1.11) - (3 * 1.33);

        assertEquals(expectedProfit, actualProfit, 0.01);

        Mockito.verify(processorMock).shipOrder(1);
        Mockito.verify(processorMock).shipOrder(2);
        Mockito.verify(processorMock).shipOrder(3);
        Mockito.verify(processorMock).shipOrder(4);
        Mockito.verify(processorMock).shipOrder(13);
        Mockito.verify(processorMock).shipOrder(1984);
        Mockito.verify(processorMock, Mockito.times(2)).getFraudulentOrders();
        Mockito.verifyNoMoreInteractions(processorMock);
        //***
        //THEN - verify expected result
        //***
    
    }

}
