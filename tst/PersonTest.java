import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

public class PersonTest {

    private CacheManager cacheMock = Mockito.mock(CacheManager.class);
    private DiskManager diskMock = Mockito.mock(DiskManager.class);

    private Person personNonExistent;
    private Person personInCache;
    private Person personInDisk;
    private int DNE_PHONE = 4444;
    private int CACHE_PHONE = 8870;
    private int DISK_PHONE = 1234;
    private String CACHE_FIRST = "Taylor";
    private String CACHE_LAST = "Swift";
    private String DISK_FIRST = "Tommy";
    private String DISK_LAST = "Thompson";

    @Before
    public void setUp() {
        personNonExistent = new Person(cacheMock, diskMock);
        personNonExistent.setPerson(DNE_PHONE, "", "");
        personInCache = new Person(cacheMock, diskMock);
        personInCache.setPerson(CACHE_PHONE, CACHE_FIRST, CACHE_LAST);
        personInDisk = new Person(cacheMock, diskMock);
        personInDisk.setPerson(DISK_PHONE, DISK_FIRST, DISK_LAST);
    }

    @Test
    public void testPersonDNE() {
        Mockito.when(cacheMock.getPerson(DNE_PHONE)).thenReturn(null);
        Mockito.when(diskMock.getPerson(DNE_PHONE)).thenReturn(null);

        Assert.assertEquals(null, cacheMock.getPerson(DNE_PHONE));
        Assert.assertEquals(null, diskMock.getPerson(DNE_PHONE));

        Mockito.verify(cacheMock, Mockito.times(1)).getPerson(DNE_PHONE);
        Mockito.verify(diskMock).getPerson(DNE_PHONE);
        Mockito.verifyNoMoreInteractions(cacheMock);
        Mockito.verifyNoMoreInteractions(diskMock);
    }

    @Test
    public void testPersonInCache() {
        Mockito.when(cacheMock.getPerson(CACHE_PHONE)).thenReturn(personInCache);
        Person actualPersonFromCache = cacheMock.getPerson(CACHE_PHONE);
        Assert.assertEquals(personInCache, actualPersonFromCache);
        Assert.assertEquals("Taylor Swift", actualPersonFromCache.getFullName());
        Mockito.verify(cacheMock, Mockito.times(2)).getPerson(CACHE_PHONE);
        Mockito.verifyNoMoreInteractions(cacheMock);
        Mockito.verifyNoMoreInteractions(diskMock);
    }

    @Test
    public void testPersonInDisk() {
        Mockito.when(cacheMock.getPerson(DISK_PHONE)).thenReturn(null);
        Mockito.when(diskMock.getPerson(DISK_PHONE)).thenReturn(personInDisk);

        Person actualPersonFromDisk = diskMock.getPerson(DISK_PHONE);

        Assert.assertEquals(personInDisk, actualPersonFromDisk);
        Assert.assertEquals("Tommy Thompson", actualPersonFromDisk.getFullName());

        Mockito.verify(diskMock, Mockito.times(2)).getPerson(DISK_PHONE);
        Mockito.verify(cacheMock).getPerson(DISK_PHONE);
        Mockito.verifyNoMoreInteractions(diskMock);
        Mockito.verifyNoMoreInteractions(cacheMock);

    }

}
