import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public class DatabaseTest {

    private static Connection connection;
    public static final String DB_URL = "jdbc:sqlite:news.sqlite";
    private static WebDriver driver;

    @BeforeClass
    public static void setUp() throws Exception {
        connection = DriverManager.getConnection(DB_URL);
        driver = new ChromeDriver();
    }

    @Test
    @Ignore
    public void testHeadlineCount() throws Exception {
        String sql = "select count(*) from headlines";
        PreparedStatement ps = connection.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        rs.next();
        int count = rs.getInt("count(*)");
        // the size of the table will not always be one
        Assert.assertEquals(1, count);
    }

    @Test
    public void testInsertHeadlines() throws Exception{
        driver.get("https://news.ycombinator.com");
        List<WebElement> spans = driver.findElements(By.className("titleline"));
        Assert.assertEquals(30, spans.size());
        for (int i = 0; i < spans.size(); i++) {
            String url = spans.get(i).findElement(By.tagName("a")).getAttribute("href");
            String headline = spans.get(i).getText();
            String sql = "insert into headlines (text, url) values (?, ?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, headline);
            ps.setString(2, url);
            ps.executeUpdate();
        }
    }


}
