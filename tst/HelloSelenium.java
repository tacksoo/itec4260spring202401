import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.time.Duration;

public class HelloSelenium {

    private static WebDriver driver;

    @BeforeClass
    public static void setup() {
        //System.setProperty("webdriver.gecko.driver","geckodriver.exe");
        //driver = new FirefoxDriver();
        System.setProperty("webdriver.chrome.driver","chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    @Ignore
    public void testGoogle() {
        driver.get("https://www.google.com");
        WebElement searchBar = driver.findElement(By.name("q"));
        searchBar.clear();
        searchBar.sendKeys("GGC");
        searchBar.submit();
        Assert.assertTrue(driver.getTitle().contains("GGC"));
    }

    @Test
    public void testBanner() {
        // https://ggc.gabest.usg.edu/
        // login with your credentials
        driver.get("https://ggc.gabest.usg.edu/pls/B400/twbkwbis.P_WWWLogin");
        //driver.findElement(By.xpath("/html/body/div[3]/table[1]/tbody/tr[4]/td[2]/a")).click();
        WebElement sid = driver.findElement(By.name("sid"));
        sid.sendKeys("900012345");
        WebElement pin = driver.findElement(By.name("PIN"));
        pin.sendKeys("010199");
        WebElement loginButton = driver.findElement(By.cssSelector("body > div.pagebodydiv > form > p > input[type=submit]"));
        loginButton.click();
    }

    @Test
    @Ignore
    public void testBing() {
        driver.get("https://www.bing.com");
    }

    @Test
    public void testGetARoom() throws Exception {
        driver.manage().window().setSize(new Dimension(600,600));
        driver.get("https://www.getaroom.com");
        WebElement destination = driver.findElement(By.cssSelector("#destination"));
        destination.clear();
        destination.sendKeys("Las Vegas");
        destination.click();
        WebElement submit = driver.findElement(By.cssSelector("#enter-travel-dates"));submit.click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        //driver.get("https://www.getaroom.com/search?amenities=&destination=Paris&page=1&per_page=25&rinfo=%5B%5B18%5D%5D&sort_order=position&hide_unavailable=true&check_in=2024-05-13&check_out=2024-05-15&property_name=");
        WebElement filterBtn = driver.findElement(By.cssSelector("#toggle_filters_btn"));
        filterBtn.click();
        WebElement hotelName = driver.findElement(By.cssSelector("#hotelName"));
        hotelName.sendKeys("hilton");

    }

}
