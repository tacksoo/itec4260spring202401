import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

@RunWith(JUnitParamsRunner.class)
public class TestingWithParams {

    private static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        driver = new ChromeDriver();
    }

    @Test
    @Ignore
    @Parameters({"itec4260,sam","stec4500,anna","chem1212k,tom"})
    public void testParams(String str, String name) {
        System.out.println(str);
    }

    @Test
    @Parameters({"spring break", "best place for spring break", "summer vacation"})
    public void testGoogleSearch(String str) throws Exception {
        driver.get("https://www.google.com");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        WebElement searchBox = driver.findElement(By.name("q"));
        searchBox.clear();
        searchBox.sendKeys(str);
        searchBox.submit();
        System.out.println(driver.getTitle());
    }



}
