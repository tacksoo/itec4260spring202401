import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

@RunWith(JUnitParamsRunner.class)
public class SocialSecurityTest {

    private static WebDriver driver;
    private String SS_URL = "https://www.ssa.gov/OACT/quickcalc/";
    @BeforeClass
    public static void setUp() {
        driver = new ChromeDriver();
    }


    @Test
    @Parameters({"1,1,1990,55000","1,1,1980,65000","1,1,1970,75000"})
    public void testSocialSecurityPayOut(String m, String d, String y, String salary) {
        driver.get("https://www.ssa.gov/OACT/quickcalc/");
        WebElement month = driver.findElement(By.name("dobmon"));
        WebElement day = driver.findElement(By.name("dobday"));
        WebElement year = driver.findElement(By.name("yob"));
        month.clear();
        month.sendKeys(m);
        day.clear();
        day.sendKeys(d);
        year.clear();
        year.sendKeys(y);
        WebElement submit = driver.findElement(By.xpath("/html/body/table[4]/tbody/tr[2]/td[2]/form/table/tbody/tr[5]/td/input"));
        submit.click();
        WebElement submit2 = driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[3]/td/form/input[10]"));
        submit2.click();
        // get the tbody with input elements first
        int startYear = Integer.parseInt(y) + 17;
        for (int i = startYear; i <= 2024; i++) {
            WebElement input = driver.findElement(By.name(String.valueOf(i)));
            input.clear();
            input.sendKeys(salary);
        }
        WebElement submit3 = driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[1]/td[1]/form/input[6]"));
        submit3.click();

        WebElement fraEstimate = driver.findElement(By.id("est_fra"));
        System.out.println(fraEstimate.getText());
    }


    @Test
    @Parameters({"150000","168800","1000000","2000000"})
    public void testMaxBenefit(String salary) {
        driver.get("https://www.ssa.gov/OACT/quickcalc/");
        WebElement month = driver.findElement(By.name("dobmon"));
        WebElement day = driver.findElement(By.name("dobday"));
        WebElement year = driver.findElement(By.name("yob"));
        month.clear();
        month.sendKeys("1");
        day.clear();
        day.sendKeys("1");
        year.clear();
        year.sendKeys("1986");  // assuming an average American
        WebElement submit = driver.findElement(By.xpath("/html/body/table[4]/tbody/tr[2]/td[2]/form/table/tbody/tr[5]/td/input"));
        submit.click();
        WebElement submit2 = driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[3]/td/form/input[10]"));
        submit2.click();
        // get the tbody with input elements first
        int startYear = 1986 + 17;
        for (int i = startYear; i <= 2024; i++) {
            WebElement input = driver.findElement(By.name(String.valueOf(i)));
            input.clear();
            input.sendKeys(salary);
        }
        WebElement submit3 = driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[1]/td[1]/form/input[6]"));
        submit3.click();

        WebElement fraEstimate = driver.findElement(By.id("est_fra"));
        System.out.println(fraEstimate.getText());
    }

    // assuming average age 38 (born in the year 1986)
    // assuming average income $60000 (in 2024)
    @Test
    @Parameters({"0","5","10","15"})
    public void testGapYears(int gapYears) {
        driver.get("https://www.ssa.gov/OACT/quickcalc/");
        WebElement month = driver.findElement(By.name("dobmon"));
        WebElement day = driver.findElement(By.name("dobday"));
        WebElement year = driver.findElement(By.name("yob"));
        month.clear();
        month.sendKeys("1");
        day.clear();
        day.sendKeys("1");
        year.clear();
        year.sendKeys("1986");  // assuming an average American
        WebElement submit = driver.findElement(By.xpath("/html/body/table[4]/tbody/tr[2]/td[2]/form/table/tbody/tr[5]/td/input"));
        submit.click();
        WebElement submit2 = driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[3]/td/form/input[10]"));
        submit2.click();
        // get the tbody with input elements first
        int startYear = 1986 + 17;
        for (int i = startYear; i <= 2024; i++) {
            WebElement input = driver.findElement(By.name(String.valueOf(i)));
            input.clear();
            if (i >= 2005 && i <= 2005 + gapYears) {
                input.sendKeys("0");
            } else {
                input.sendKeys("60000");
            }
        }
        WebElement submit3 = driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[1]/td[1]/form/input[6]"));
        submit3.click();

        WebElement fraEstimate = driver.findElement(By.id("est_fra"));
        System.out.println(fraEstimate.getText());
    }


}
