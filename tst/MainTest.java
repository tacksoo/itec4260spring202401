import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

@RunWith(MockitoJUnitRunner.class)
public class MainTest {

    @Mock
    private MyFancyFileWriter mockWriter;

    @Spy
    private MyFancyFileWriter spyWriter;

    @Test
    public void testDownloadJSON() throws Exception {
        // in processing json
        // 1. create an ObjectMapper object
        ObjectMapper mapper = new ObjectMapper();
        // 2. download the json file from somewhere
        JsonNode root = mapper.readTree(new URL("https://store.steampowered.com/api/appdetails?appids=1966720"));
        String name = root.get("1966720").get("data").get("name").asText();
        System.out.println(name);
        // how to get genres
        JsonNode genresNode = root.get("1966720").get("data").get("genres");
        for (int i = 0; i < genresNode.size(); i++) {
            System.out.println(genresNode.get(i).get("description").asText());
        }
        // how to get developers
    }

    @Test
    public void testFavGames() {
        // create an array of your three favorite games from Steam
        // steamdb.info
        String [] appIDs = { "1966720", "1086940", "4700"};
        Game [] myGames = new Game[3];
        for (int i = 0; i < appIDs.length; i++) {
            Game g = GameFactory.getGameFromSteam(appIDs[i]);
            g.printGame();
        }
    }

    private Game getGame(String appID) {
        ObjectMapper mapper = new ObjectMapper();
        Game g = new Game(appID);
        try {
            URL url = new URL("https://store.steampowered.com/api/appdetails?appids=" + appID);
        } catch(MalformedURLException  e) {
            e.printStackTrace();
        }
        return g;
    }

    @Test
    public void testAddition() {
        Assert.assertEquals(2, Main.add(1, 1));
        System.out.println(Integer.MAX_VALUE);
    }

    @Test
    public void printArray() {
        int[] nums = {1,2,3,4,5};
        System.out.println(Arrays.toString(nums));
    }

    public double getWage(int hours, int overtime) {
        return hours*18.5 + overtime*20.5;
    }

    @Test
    public void testGetWage() {
        //System.out.println(getWage(20,1));
        Assert.assertEquals(390,getWage(20,1),1);
    }

    @Test(expected = ArithmeticException.class)
    public void testArithemeticException() {
        int x = 1/0;
    }

    @Test
    public void testGameToJSON() {
        Game g = GameFactory.getGameFromSteam("1623730");
        System.out.println(GameFactory.getJSONStringOfGame(g));
    }

    @Test
    public void testDownloadFile() throws Exception {
        URL url = new URL("https://gist.githubusercontent.com/tacksoo/6835a34b401997abef8faea4d05a7593/raw/13dbfb10fb8b335fc813eaad7d93a749b57426e5/modely.csv");
        String data = IOUtils.toString(url);
        System.out.println(data);
        CSVParser parser = new CSVParser(new StringReader(data), CSVFormat.DEFAULT.builder().setHeader().build());
        ArrayList<String> dates = new ArrayList<>();
        ArrayList<Integer> prices = new ArrayList<>();
        for (CSVRecord record: parser) {
            dates.add(record.get("Date"));
            String dollar = record.get("LR-AWD").replace(",","").replace("$","");
            prices.add(Integer.parseInt(dollar));
        }
        System.out.println(dates);
        System.out.println(prices);
        Assert.assertTrue(dates.size() == prices.size());
        // get cheapest price and date
        int index = getSmallestNumIndex(prices);
        System.out.println(dates.get(index) + " " + prices.get(index));
    }

    private int getSmallestNumIndex(ArrayList<Integer> prices) {
        int index = 0;
        int cheapest = Integer.MAX_VALUE;
        for (int i = 0; i < prices.size(); i++) {
            if (prices.get(i) < cheapest) {
                cheapest = prices.get(i);
                index = i;
            }
        }
        return index;
    }



    @Test
    public void printNames() throws Exception {
        Scanner scanner = new Scanner(new File("names.txt"));
        //while (scanner.hasNextLine()) {
            //System.out.println(scanner.nextLine());
        //}
        //String str = FileUtils.readFileToString(new File("names.txt"),"UTF-8");
        //System.out.println(str);
        FileUtils.writeStringToFile(new File("names.txt"),"\nGabe\n","UTF-8",true);
        FileUtils.writeStringToFile(new File("names.txt"),"\nRavjot\n","UTF-8",true);

    }

    @Test
    public void shuffleNames() throws Exception {
        List<String> names = FileUtils.readLines(new File("names.csv"),"UTF-8");
        Collections.shuffle(names);
        for (int i = 0; i < names.size(); i++) {
            System.out.println(names.get(i));
        }
    }

    @Test
    public void testMockvsSpy() {
        mockWriter.writeStringToFile("hello world");
        Mockito.verify(mockWriter).writeStringToFile("hello world");

        spyWriter.writeStringToFile("hello world");
        Mockito.verify(spyWriter).writeStringToFile("hello world");
    }


}
