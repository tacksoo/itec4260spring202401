import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class InventoryTest {
    private Inventory myInventory;

    @Before
    public void setUp() {
        myInventory = new Inventory();
        String [] appIDs = { "1966720", "1086940", "4700"};
        Game [] myGames = new Game[3];
        for (int i = 0; i < appIDs.length; i++) {
            Game g = GameFactory.getGameFromSteam(appIDs[i]);
            System.out.println(g.getName());
            System.out.println(g.getRating());
            myInventory.add(g);
        }
    }

    @Test
    public void testAddGame() {
        Assert.assertEquals(3, myInventory.getSize());
        Game favGame = GameFactory.getGameFromSteam("270880");
        myInventory.add(favGame);
        // testing the addition of one and one more game
        Assert.assertEquals(4, myInventory.getSize());
        Game secondFavGame = GameFactory.getGameFromSteam("427520");
        myInventory.add(secondFavGame);
        Assert.assertEquals(5, myInventory.getSize());
        // test a duplicate game
        myInventory.add(favGame);
        Assert.assertEquals(5, myInventory.getSize());
        // hw: change a field of a duplicate game and see if it is getting replaced
    }

    @Test
    public void testRemoveGame() {
        Game lethalCompany = GameFactory.getGameFromSteam("1966720");
        myInventory.remove(lethalCompany);
        Assert.assertEquals("Removed one game from inventory of three",2, myInventory.getSize());
        myInventory.remove(lethalCompany);
        Assert.assertEquals("Removing a game that was already removed",2, myInventory.getSize());
        Game g1 = GameFactory.getGameFromSteam("1086940");
        Game g2 = GameFactory.getGameFromSteam("4700");
        myInventory.remove(g1);
        myInventory.remove(g2);
        Assert.assertEquals(0,myInventory.getSize());
    }

    @Test
    public void testPrintAveragePrice() {
        // average price of games in my inventory
        double expected =  (999 + 5999 + 2499)/3.0;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);
        myInventory.printAveragePriceOfAllGames();
        double actual = Double.parseDouble(baos.toString());
        Assert.assertEquals(expected, actual, 0.01);
    }

    @Test
    public void testFindCheapestGame() {
        Game cheapest = myInventory.findCheapestGame();
        Assert.assertEquals("Lethal Company", cheapest.getName());
        Game vampires = GameFactory.getGameFromSteam("1794680");  // vampire survivors 5.99
        myInventory.add(vampires);
        cheapest = myInventory.findCheapestGame();
        Assert.assertEquals("Vampire Survivors", cheapest.getName());
    }

    @Test
    public void testfindHighestRatedGame() {
        Game g = myInventory.findMostHighlyRatedGame();
        Assert.assertEquals("Baldur's Gate 3", g.getName());
    }
}
