import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;

public class SlashDotTest {

    private static Connection connection;
    private static WebDriver driver;
    public static String DB_URL = "jdbc:sqlite:slashdot.sqlite";
    @BeforeClass
    public static void setUp() throws Exception {
        connection = DriverManager.getConnection(DB_URL);
        driver = new ChromeDriver();
    }

    @Test
    @Ignore
    public void insertTest() throws Exception {
        String sql = "insert into headlines (headline, url) values (?, ?)";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, "testing 1 2 3 ...");
        ps.setString(2, "https://www.testing.com");
        ps.executeUpdate();
    }

    @Test
    public void testHeadlines() throws Exception {
        int startTableCount = getHeadLinesCount();
        driver.get("https://www.slashdot.org");
        List<WebElement> titles = driver.findElements(By.className("story-title"));
        String sql = "insert into headlines (headline, url) values (?, ?)";
        PreparedStatement ps = connection.prepareStatement(sql);

        for (int i = 0; i < titles.size(); i++) {
            String headline = titles.get(i).getText();
            String url = titles.get(i).findElement(By.tagName("a")).getAttribute("href");
            ps.setString(1, headline);
            ps.setString(2, url);
            ps.executeUpdate();
        }
        int endTableCount = getHeadLinesCount();
        // assert the fact that the table count increases by 15
        Assert.assertEquals(15, endTableCount-startTableCount);
    }

    public int getHeadLinesCount() throws Exception {
        String sql = "select count(*) from headlines";
        PreparedStatement ps = connection.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getInt("count(*)");
    }

    @Test
    @Ignore
    public void testDate() {
        System.out.println(new Date());
    }



}
