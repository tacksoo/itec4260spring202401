import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

public class InventoryMockTest {

    @Test
    public void testCheapestGameMock() {
        InventoryInterface invMock = Mockito.mock(InventoryInterface.class);
        ArrayList<String> genres = new ArrayList<>();
        genres.add("sports");
        genres.add("simulation");
        Game madden = new Game("41241241","PC","Madden NFL","01/01/2024","EA Games", genres , 60, 10 );
        Mockito.when(invMock.findCheapestGame()).thenReturn(madden);
        Assert.assertEquals("Madden NFL", invMock.findCheapestGame().getName());
    }

    @Test
    public void testMostExpensiveGameMock() {
        InventoryInterface invMock = Mockito.mock(InventoryInterface.class);
        ArrayList<String> genres = new ArrayList<>();
        genres.add("2d platformer");
        Game mario = new Game("12345", "Console", "Super Mario Bros", "01/01/1984", "Nintendo", genres, 60, 10);
        Mockito.when(invMock.findMostExpensiveGame()).thenReturn(mario);
        Assert.assertEquals("Super Mario Bros", invMock.findMostExpensiveGame().getName());
    }


    @Test
    public void testAveragePriceOfAllGames() {
        InventoryInterface invMock = Mockito.mock(InventoryInterface.class);
        Mockito.when(invMock.getAveragePriceOfAllGames()).thenReturn(60.0);
        Assert.assertEquals(60.0, invMock.getAveragePriceOfAllGames(), 0.1);
    }
}
