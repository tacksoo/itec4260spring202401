import java.util.ArrayList;

public class Game {

    private String appID;
    private String platform;  // csv of supported platforms
    private String name;
    private String releasedDate;

    private String developer;
    private ArrayList<String> genres;
    private int retailPrice;
    private int rating;

    public Game(String appID) {
        this.appID = appID;
    }

    public Game(String appID, String platform, String name, String releasedDate, String developer, ArrayList<String> genres, int retailPrice, int rating) {
        this.appID = appID;
        this.platform = platform;
        this.name = name;
        this.releasedDate = releasedDate;
        this.developer = developer;
        this.genres = genres;
        this.retailPrice = retailPrice;
        this.rating = rating;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }


    public String getAppID() {
        return appID;
    }

    public void setAppID(String appID) {
        this.appID = appID;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReleasedDate() {
        return releasedDate;
    }

    public void setReleasedDate(String releasedDate) {
        this.releasedDate = releasedDate;
    }

    public ArrayList<String> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<String> genres) {
        this.genres = genres;
    }

    public int getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(int retailPrice) {
        this.retailPrice = retailPrice;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void printGame() {
        System.out.println(platform);
        System.out.println(name);
        System.out.println(releasedDate);
        System.out.println(developer);
        System.out.println(genres);
        System.out.println("$" + retailPrice/100);
        System.out.println(rating);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Game) {
            Game g = (Game) obj;
            if (g.getAppID().equals(this.appID)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
