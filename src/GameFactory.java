import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GameFactory {

    public static String STEAM_API_URL = "https://store.steampowered.com/api/appdetails?appids=";
    // this suffix will make sure you only get US based results
    public static String STEAM_SUFFIX = "&filters=&cc=US&l=english";

    public static Game getGameFromSteam(String appID) {
        ObjectMapper mapper = new ObjectMapper();
        Game g = null;
        try {
            JsonNode root = mapper.readTree(new URL(STEAM_API_URL + appID + STEAM_SUFFIX));
            String title = root.get(appID).get("data").get("name").asText();
            boolean pc = root.get(appID).get("data").get("platforms").get("windows").asBoolean();
            boolean mac = root.get(appID).get("data").get("platforms").get("mac").asBoolean();
            boolean linux = root.get(appID).get("data").get("platforms").get("linux").asBoolean();
            String platforms = "";
            if (pc)
                platforms += "Windows,";
            if (mac)
                platforms += "Mac,";
            if (linux)
                platforms += "Linux,";
            if (platforms.endsWith(","))
                platforms = platforms.substring(0, platforms.length()-1);
            String relasedDate = root.get(appID).get("data").get("release_date").get("date").asText();
            String developer = root.get(appID).get("data").get("developers").get(0).asText();
            String genres = root.get(appID).get("data").get("genres").get(0).get("description").asText();
            int retailPrice = root.get(appID).get("data").get("price_overview").get("final").asInt();
            int rating = root.get(appID).get("data").get("recommendations").get("total").asInt();
            ArrayList<String> genresList = new ArrayList<String>();
            g = new Game(appID, platforms, title, relasedDate, developer, genresList,retailPrice,rating);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return g;
    }


    public static String getJSONStringOfGame(Game g) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(g);
        } catch (JsonProcessingException e){
            e.printStackTrace();
        }
        return null;
    }


}
