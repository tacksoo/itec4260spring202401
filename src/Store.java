import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Store {

    private Inventory inventory;

    public Store(Inventory inv) {
        inventory = inv;
    }

    public void loadInventoryFromWeb(String url) {
        try {
            URL u = new URL(url);
            String str = IOUtils.toString(u);
            CSVParser parser = new CSVParser(new StringReader(str), CSVFormat.DEFAULT.builder().setHeader().build());
            for(CSVRecord record: parser) {
                Game g = createGameFromRecord(record);
                inventory.add(g);
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public Game createGameFromRecord(CSVRecord r) {
        //appID,platform,name,releaseDate,developer,genre,retailPrice,recommendationCount
        String appID = r.get("appID");
        String platform = r.get("platform");
        String name = r.get("name");
        String releaseDate = r.get("releaseDate");
        String developer = r.get("developer");
        ArrayList<String> genre = new ArrayList<>();
        genre.add(r.get("genre"));
        int retailPrice = Integer.parseInt(r.get("retailPrice"));
        int recommendationCount = Integer.parseInt(r.get("recommendationCount"));
        Game game = new Game(appID,platform,name,releaseDate, developer, genre,retailPrice,recommendationCount);
        return game;
    }


}
