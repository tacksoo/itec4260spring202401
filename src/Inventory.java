import java.util.ArrayList;
import java.util.List;

public class Inventory {

    private List<Game> games = new ArrayList<>();

    public Inventory() {
    }

    public int getSize() {
        return games.size();
    }

    public void add(Game g) {
        // we will overwrite existing game with the same appId
        for (int i = 0; i < games.size(); i++) {
            if (games.get(i).getAppID().equals(g.getAppID())) {
                // when duplicate found, overwrite with new game
                games.set(i, g);
                return; // found duplicate, exit method
            }
        }
        games.add(g);
    }

    public void remove(Game g) {
        for (int i = 0; i < games.size(); i++) {
            if (g.getAppID().equals(games.get(i).getAppID())) {
                games.remove(i);
            }
        }
    }

    public void printAveragePriceOfAllGames() {
        double total = 0;
        for (int i = 0; i < games.size(); i++) {
            total += games.get(i).getRetailPrice();
        }
        System.out.println(total/games.size());
    }

    public Game findCheapestGame() {
        //check empty inventory
        if (games.size() == 0) {
            return null;
        }
        Game cheapest = games.get(0);
        for (int i = 0; i < games.size(); i++) {
            if (games.get(i).getRetailPrice() < cheapest.getRetailPrice()) {
                cheapest = games.get(i);
            }
        }
        return cheapest;
    }

    public Game findMostHighlyRatedGame() {
        if (games.size() == 0) {
            return null;
        }
        Game highest = games.get(0);
        for (int i = 1; i < games.size(); i++) {
            if (highest.getRating() < games.get(i).getRating()) {
                highest = games.get(i);
            }
        }
        return highest;
    }

}
