import org.apache.commons.io.FileUtils;

import java.io.File;

public class MyFancyFileWriter {

    public void writeStringToFile(String str) {
        try {
            FileUtils.writeStringToFile(new File("myfile.txt"), str, "UTF-8");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
